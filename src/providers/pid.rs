use std::{borrow::Cow, collections::HashMap};

use serde::{Deserialize, Serialize};
use serde_repr::Deserialize_repr;

use super::{GenericLocation, GenericResponse, GenericResponseType};

pub const URL: &str = "https://mapa.pid.cz/getData.php";

#[derive(Debug, Serialize)]
#[serde(rename = "snakeCase")]
pub struct MapaRequest {
    pub action: Cow<'static, str>,
    pub bounds: Vec<f64>,
    pub opened_vehical_window: bool,
}

#[derive(Debug, Deserialize)]
pub struct MapaResponse {
    pub trips: HashMap<i64, MapaResponseTrip>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MapaResponseTrip {
    pub lat: f64,
    pub lon: f64,
    pub trip_id: Option<String>,
    pub route: String,
    pub route_type: MapaRouteType,
    pub bearing: Option<i64>,
    pub delay: i64,
    pub inactive: bool,
    pub state_position: Option<String>,
    pub vehicle: String,
}

#[derive(Debug, Clone, Copy, Deserialize_repr)]
#[repr(u64)]
pub enum MapaRouteType {
    Tram = 0,
    Train = 2,
    Bus = 3,
    Ferry = 4,
}

pub async fn get_data(x: f64, y: f64) -> Result<Vec<GenericResponse>, reqwest::Error> {
    for iter in 1..16 {
        let scale_change = 0.00125 * iter as f64;

        let location = GenericLocation {
            x0: x - scale_change,
            y0: y - scale_change,
            x1: x + scale_change,
            y1: y + scale_change,
        };

        let res: MapaResponse = reqwest::Client::builder()
            .user_agent(concat!(
                env!("CARGO_PKG_NAME"),
                "/",
                env!("CARGO_PKG_VERSION")
            ))
            .build()?
            .post(URL)
            .json(&MapaRequest {
                action: Cow::Borrowed("getData"),
                bounds: vec![location.y0, location.x0, location.y1, location.x1],
                opened_vehical_window: false,
            })
            .send()
            .await?
            .json()
            .await?;

        let locations = res
            .trips
            .into_iter()
            .map(|(_, trip)| GenericResponse {
                x: trip.lat,
                y: trip.lon,
                r#type: match trip.route_type {
                    MapaRouteType::Tram => GenericResponseType::Tram,
                    MapaRouteType::Train => GenericResponseType::Train,
                    MapaRouteType::Bus => GenericResponseType::Bus,
                    MapaRouteType::Ferry => GenericResponseType::Ferry,
                },
                delay_seconds: Some(trip.delay),
                route_name: trip.route,
                provider: super::Provider::Pid,
                info_url: None,
            })
            .collect::<Vec<_>>();
        let empty = locations.is_empty();

        if !empty {
            return Ok(locations);
        }
    }

    Ok(Vec::new())
}
