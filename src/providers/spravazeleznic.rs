use std::borrow::Cow;

use proj::{Proj, ProjCreateError, ProjError};
use serde::{Deserialize, Serialize};

use super::GenericResponse;

pub const URL: &str =
    "https://mapy.spravazeleznic.cz/serverside/request2.php?module=Layers\\OsVlaky&action=load";
// SŽ's API uses the Krovak East North coordinate reference system
pub const CRS: &str = "EPSG:5514";

#[derive(Debug, Clone, Deserialize)]
pub struct SpravaZeleznicResult {
    pub success: bool,
    #[serde(default)]
    pub result: Vec<SpravaZeleznicResponse>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct SpravaZeleznicResponse {
    pub geometry: SpravaZeleznicResponseGeo,
    pub properties: SpravaZeleznicResponseProperties,
}

#[derive(Debug, Clone, Deserialize)]
pub struct SpravaZeleznicResponseProperties {
    #[serde(rename = "tt")]
    pub train_type: String,
    #[serde(rename = "tn")]
    pub train_number: String,
    #[serde(rename = "fn")]
    pub start: String,
    #[serde(rename = "ln")]
    pub end: String,
    #[serde(rename = "nsn")]
    pub next_stop: String,
    #[serde(rename = "pde")]
    pub delay: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct SpravaZeleznicResponseGeo {
    pub coordinates: [f64; 2],
}

#[derive(Debug, Clone, Serialize)]
pub struct SpravaZeleznicRequest {
    pub module: Cow<'static, str>,
    pub action: Cow<'static, str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub search: Option<Cow<'static, str>>,
}

#[derive(Debug)]
pub enum SpravaZeleznicError {
    Reqwest(reqwest::Error),
    ProjCreate,
    Proj,
    ApiError,
}

impl From<reqwest::Error> for SpravaZeleznicError {
    fn from(e: reqwest::Error) -> Self {
        Self::Reqwest(e)
    }
}

impl From<ProjError> for SpravaZeleznicError {
    fn from(e: ProjError) -> Self {
        tracing::error!("ProjError: {}", e);
        Self::Proj
    }
}

thread_local! {
    static SZ_TO_G_PROJ: Result<Proj, ProjCreateError> = Proj::new_known_crs(CRS, super::GENERIC_CRS, None);
    static G_TO_SZ_PROJ: Result<Proj, ProjCreateError> = Proj::new_known_crs(super::GENERIC_CRS, CRS, None);
}

pub async fn get_data(x: f64, y: f64) -> Result<Vec<GenericResponse>, SpravaZeleznicError> {
    let res: SpravaZeleznicResult = reqwest::Client::builder()
        .user_agent(concat!(
            env!("CARGO_PKG_NAME"),
            "/",
            env!("CARGO_PKG_VERSION")
        ))
        .build()?
        .post(URL)
        .form(&SpravaZeleznicRequest {
            action: Cow::Borrowed("load"),
            module: Cow::Borrowed("Layers\\OsVlaky"),
            search: None,
        })
        .send()
        .await?
        .json()
        .await?;

    match res.success {
        true => {
            let mut possibilities = Vec::new();

            for iter in 1..16 {
                let scale_change = 0.00125 * iter as f64;
                let (sz_x0, sz_y0) = G_TO_SZ_PROJ.with(|proj| {
                    proj.as_ref()
                        .map(|proj| proj.convert((y - scale_change, x - scale_change)))
                        .map_err(|_| SpravaZeleznicError::ProjCreate)
                })??;
                let (sz_x1, sz_y1) = G_TO_SZ_PROJ.with(|proj| {
                    proj.as_ref()
                        .map(|proj| proj.convert((y + scale_change, x + scale_change)))
                        .map_err(|_| SpravaZeleznicError::ProjCreate)
                })??;

                for train_location in &res.result {
                    let (pos_x, pos_y) = (
                        train_location.geometry.coordinates[0],
                        train_location.geometry.coordinates[1],
                    );

                    if (pos_x >= sz_x0 && pos_y >= sz_y0) && (pos_x <= sz_x1 && pos_y <= sz_y1) {
                        let (g_x, g_y) = SZ_TO_G_PROJ.with(|proj| {
                            proj.as_ref()
                                .map(|proj| proj.convert((pos_y, pos_x)))
                                .map_err(|_| SpravaZeleznicError::ProjCreate)
                        })??;
                        let route_name = format!(
                            "{}{}",
                            train_location.properties.train_type,
                            train_location.properties.train_number
                        );

                        possibilities.push(GenericResponse {
                            route_name: route_name.clone(),
                            provider: super::Provider::SpravaZeleznic,
                            x: g_x,
                            y: g_y,
                            r#type: super::GenericResponseType::Train,
                            delay_seconds: train_location
                                .properties
                                .delay
                                .ends_with(" min")
                                .then(|| {
                                    train_location.properties.delay[.." min".len()]
                                        .parse::<i64>()
                                        .ok()
                                })
                                .flatten()
                                .map(|x| x * 60),
                            info_url: Some(format!(
                                "https://mapy.spravazeleznic.cz/vlaky/{}",
                                route_name
                            )),
                        });
                    }
                }

                if !possibilities.is_empty() {
                    break;
                }
            }

            Ok(possibilities)
        }
        false => {
            tracing::error!("SpravaZeleznic provider returned an error");
            Err(SpravaZeleznicError::ApiError)
        }
    }
}
