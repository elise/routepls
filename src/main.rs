pub mod api;
pub mod frontend;
pub mod providers;

use axum::{routing::get, Router};
use http::Method;
use tower_http::cors::{Any, CorsLayer};

#[tokio::main]
async fn main() {
    tracing::subscriber::set_global_default(tracing_subscriber::FmtSubscriber::new()).unwrap();

    tracing::info!("Meow!");

    let router = Router::new()
        .route("/", get(frontend::html))
        .route("/meow.css", get(frontend::css))
        .route(
            "/api/v1/routepls",
            get(api::v1::handler).route_layer(
                CorsLayer::new()
                    .allow_origin(Any)
                    .allow_methods([Method::GET]),
            ),
        );

    axum::Server::bind(&"0.0.0.0:80".parse().unwrap())
        .serve(router.into_make_service())
        .await
        .unwrap();
}
