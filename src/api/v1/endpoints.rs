use std::collections::HashMap;

use axum::{extract::Query, Json};

use crate::providers::{GenericResponse, Provider};

use super::{ApiError, RequestQuery};

pub async fn handler(query: Query<RequestQuery>) -> Result<Json<Vec<GenericResponse>>, ApiError> {
    let query = query.0;

    let providers = match query.providers {
        Some(mut providers) => {
            providers.dedup();
            providers
        }
        None => {
            vec![Provider::Pid, Provider::SpravaZeleznic]
        }
    };

    let mut res = HashMap::new();

    for provider in providers {
        if !res.is_empty() {
            break;
        }

        fn make_key(res: &GenericResponse) -> String {
            format!("{:#?}{}", res.r#type, res.route_name)
        }

        match provider {
            Provider::Pid => {
                for x in crate::providers::pid::get_data(query.x, query.y).await? {
                    let prov = x.provider;
                    if let Some(old) = res.insert(make_key(&x), x) {
                        if old.provider > prov {
                            res.insert(make_key(&old), old);
                        }
                    }
                }
            }
            Provider::Vbb => unimplemented!(),
            Provider::SpravaZeleznic => {
                for x in crate::providers::spravazeleznic::get_data(query.x, query.y).await? {
                    let prov = x.provider;
                    if let Some(old) = res.insert(make_key(&x), x) {
                        if old.provider > prov {
                            res.insert(make_key(&old), old);
                        }
                    }
                }
            }
        }
    }

    Ok(Json(res.into_values().collect()))
}
