use std::borrow::Cow;

use axum::{response::IntoResponse, Json};
use http::StatusCode;
use serde::{Deserialize, Serialize};

use crate::providers::{spravazeleznic::SpravaZeleznicError, Provider};

#[derive(Debug, Deserialize)]
pub struct RequestQuery {
    pub x: f64,
    pub y: f64,
    pub providers: Option<Vec<Provider>>,
}

pub enum ApiError {
    InternalError(Cow<'static, str>),
}

impl From<reqwest::Error> for ApiError {
    fn from(err: reqwest::Error) -> Self {
        tracing::error!("Making request failed: {}", err);
        Self::InternalError(Cow::Borrowed("Failed to make request"))
    }
}

impl From<SpravaZeleznicError> for ApiError {
    fn from(_: SpravaZeleznicError) -> Self {
        Self::InternalError(Cow::Borrowed("SŽ returned an error"))
    }
}

#[derive(Debug, Serialize)]
pub struct ApiErrorResponse {
    pub status: u16,
    pub error: &'static str,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub detailed_message: Option<Cow<'static, str>>,
}

impl IntoResponse for ApiError {
    fn into_response(self) -> axum::response::Response {
        let (status, error, detailed_message) = match self {
            ApiError::InternalError(error) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                "INTERNAL_SERVER_ERROR",
                Some(error),
            ),
        };

        let mut res = Json(ApiErrorResponse {
            status: status.as_u16(),
            error,
            detailed_message,
        })
        .into_response();
        if res.status() == StatusCode::OK {
            *res.status_mut() = status;
        }
        res
    }
}
