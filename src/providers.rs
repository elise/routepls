pub mod pid;
pub mod spravazeleznic;

use serde::{Deserialize, Serialize};

// The coordinate system we use for generic coordinates
pub const GENERIC_CRS: &str = "EPSG:4326";

#[derive(Debug, PartialEq, Eq, Clone, Copy, Serialize)]
pub enum GenericResponseType {
    Train,
    Underground,
    Tram,
    Bus,
    Ferry,
    Other,
}

#[derive(Debug, Serialize)]
pub struct GenericResponse {
    pub route_name: String,
    pub provider: Provider,
    pub x: f64,
    pub y: f64,
    pub r#type: GenericResponseType,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub delay_seconds: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub info_url: Option<String>,
}

#[derive(Debug, PartialEq, Clone, Serialize)]
pub struct GenericLocation {
    pub x0: f64,
    pub y0: f64,
    pub x1: f64,
    pub y1: f64,
}

/// Providers are ordered in their preference from worst to best
#[derive(Debug, PartialEq, Eq, Clone, Copy, Serialize, Deserialize, PartialOrd, Ord)]
pub enum Provider {
    // Germany
    Vbb,
    // Czechia
    SpravaZeleznic,
    Pid,
}
