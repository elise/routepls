use axum::response::Html;
use http::{header::HeaderName, HeaderValue};

pub async fn html() -> Html<&'static str> {
    Html(include_str!("meow.html"))
}

pub async fn css() -> ([(HeaderName, HeaderValue); 1], &'static str) {
    (
        [(
            http::header::CONTENT_TYPE,
            HeaderValue::from_static("text/css"),
        )],
        include_str!("meow.css"),
    )
}
