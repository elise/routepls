FROM rust:1.64

EXPOSE 80

WORKDIR /usr/src/routepls
COPY . .

RUN apt update && apt install clang cmake sqlite3 -y
RUN cargo install --path .
CMD ["routepls"]
